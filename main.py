from send_mail import send_mail_ip
import requests
import os
import smtplib

# Constants
# Constants
FILE_PATH = 'prev_ip.txt'


def get_public_ip():
    try:
        # Fetch the public IP using the ipify API
        response = requests.get('https://api.ipify.org/')
        data = response.text
        return data
    except Exception as e:
        print(f'Some error: {e}')
        return None

def write_prev_ip_file(current_ip):
    # Write the current IP to the prev_ip.txt file
    with open(FILE_PATH, 'w') as file:
        file.write(current_ip)

def check_prev_ip():
    # Get the current public IP
    current_ip = get_public_ip()

    # Check if the file exists
    if not os.path.exists(FILE_PATH):
        # If the file doesn't exist, create one and write the current IP to it
        write_prev_ip_file(current_ip)
        send_mail_ip(current_ip)
        return

    # If the file exists, read the previous IP from the file
    with open(FILE_PATH, 'r') as file:
        prev_ip = file.read().strip()

    # Compare the previous IP with the current IP
    if prev_ip != current_ip:
        # If they are different, update the file and send an email
        write_prev_ip_file(current_ip)
        send_mail_ip(current_ip)

def main():
    # Entry point of the script
    check_prev_ip()

if __name__ == '__main__':
    main()
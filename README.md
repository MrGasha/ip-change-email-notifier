# Description

An IP change email notifier for poor's man DDNS. This creates a prev_ip.txt files and compares current IP  to it, if there is a change it sends an email and stores current IP

## Usage

first install requirements using:
```bash
pip install -r requirements.txt
```
after that you need to configure the constants at send_mail.py with your own values as follows:
```python
SENDER_EMAIL = 'youremail@example.com'
SENDER_PASSWORD = 'youpassword'
RECIPIENT_EMAIL = 'recipient@example.com'
SMTP_SERVER = 'smtp.example.com'
SMTP_PORT = 587
```
finally you can run the script by running

```bash
python main.py
```
For usability you can schedule a cron job or a task on windows to execute the script every 5 mins or whatever suits your necessity. <br/>
On linux: 
```bash
crontab -e
```
Then add this entry:
```bash
*/5 * * * * /path/to/your/virtualenv/bin/python /path/to/main.py
```





import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

#Constants
SENDER_EMAIL = 'youremail@example.com'
SENDER_PASSWORD = 'youpassword'
RECIPIENT_EMAIL = 'recipient@example.com'
SMTP_SERVER = 'smtp.example.com'
SMTP_PORT = 587

def send_mail_ip(current_ip):
    subject = 'Beep bop: IP Address has Change'
    body = """
        Beep Bop: The IP address has changed. 
        This is the New IP:{}
        Thank me later human.
    """.format(current_ip)

    message = MIMEMultipart()
    message['From'] = SENDER_EMAIL
    message['To'] = RECIPIENT_EMAIL
    message['Subject'] = subject
    message.attach(MIMEText(body, 'plain'))

    try:
        with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
            server.starttls()
            server.login(SENDER_EMAIL, SENDER_PASSWORD)
            text = message.as_string()
            server.sendmail(SENDER_EMAIL, RECIPIENT_EMAIL, text)
        print('Email sent successfully.')

    except Exception as e:
        print(f'Error sending email: {e}')
